/*
getAll - obtener todos los registros - filtros opcionales de busqueda deben estar por
cada campo de la tabla
getById - obtener un registro por Id
getByField - obtener un registro por campo arbitrario
create - crear un registro
update - actualizar registro
delete - borrar un registro
*/
const db = require('../db/conexion');
const Color = require('color');

// Mapa de colores en español a inglés
const coloresEspañolAIngles = {
    rojo: 'red',
    azul: 'blue',
    verde: 'green',
    amarillo: 'yellow',
    negro: 'black',
    blanco: 'white',
    morado: 'purple',
    naranja: 'orange',
    rosa: 'pink',
    gris: 'gray',
    marrón: 'brown',
    cian: 'cyan',
    lima: 'lime',
    magenta: 'magenta',
    plata: 'silver',
    dorado: 'gold',
    // Añade más colores según sea necesario
};

function nombreAHexadecimal(nombreColor) {
    try {
        const color = Color(nombreColor.toLowerCase());
        return color.hex();
    } catch (e) {
        const nombreIngles = coloresEspañolAIngles[nombreColor.toLowerCase()];
        if (nombreIngles) {
            try {
                const color = Color(nombreIngles);
                return color.hex();
            } catch (e) {
                return null;
            }
        } else {
            return null;
        }
    }
}

const ColoresModel = {
    getAll: function(queryParams) {
        let sql = "SELECT * FROM Colores WHERE activo = 1";
        let queryParamsArray = [];

        if (queryParams.id) {
            sql += " AND integranteId = ?";
            queryParamsArray.push(queryParams.id);
        }
        if (queryParams.background) {
            const hexColor = nombreAHexadecimal(queryParams.background);
            if (hexColor) {
                sql += " AND background = ?";
                queryParamsArray.push(hexColor);
            } else {
                sql += " AND (background LIKE ? OR background LIKE ?)";
                queryParamsArray.push('%' + queryParams.background + '%', '%' + queryParams.background.toLowerCase() + '%');
            }
        }
        if (queryParams.headerBackground) {
            const hexColor = nombreAHexadecimal(queryParams.headerBackground);
            if (hexColor) {
                sql += " AND headerBackground = ?";
                queryParamsArray.push(hexColor);
            } else {
                sql += " AND (headerBackground LIKE ? OR headerBackground LIKE ?)";
                queryParamsArray.push('%' + queryParams.headerBackground + '%', '%' + queryParams.headerBackground.toLowerCase() + '%');
            }
        }
        if (queryParams.sectionBackground) {
            const hexColor = nombreAHexadecimal(queryParams.sectionBackground);
            if (hexColor) {
                sql += " AND sectionBackground = ?";
                queryParamsArray.push(hexColor);
            } else {
                sql += " AND (sectionBackground LIKE ? OR sectionBackground LIKE ?)";
                queryParamsArray.push('%' + queryParams.sectionBackground + '%', '%' + queryParams.sectionBackground.toLowerCase() + '%');
            }
        }

        return new Promise((resolve, reject) => {
            db.all(sql, queryParamsArray, (err, results) => {
                if (err) {
                    console.error('Error al obtener datos:', err);
                    reject('Error al obtener datos de la base de datos');
                }
                resolve(results);
            });
        });
    },

    getById: function(id) {
        return new Promise((resolve, reject) => {
            db.get('SELECT * FROM Colores WHERE integranteId = ?', [id], (err, row) => {
                if (err) {
                    console.error('Error al obtener datos:', err);
                    reject('Error al obtener datos de la base de datos');
                }
                resolve(row);
            });
        });
    },

    getByField: function(fieldName, value) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM Colores WHERE ${fieldName} = ?`, [value], (err, row) => {
                if (err) {
                    console.error('Error al obtener datos:', err);
                    reject('Error al obtener datos de la base de datos');
                }
                resolve(row);
            });
        });
    },

    create: function(data) {
        const { integranteId, background, headerBackground, sectionBackground } = data;

        return new Promise((resolve, reject) => {
            const query = `INSERT INTO Colores (integranteId, background, headerBackground, sectionBackground) VALUES (?, ?, ?, ?)`;
            db.run(query, [integranteId, background, headerBackground, sectionBackground], function(err) {
                if (err) {
                    reject('Error al insertar en la base de datos');
                }
                resolve(this.lastID);
            });
        });
    },

    update: function(id, newData) {
        const { background, headerBackground, sectionBackground, activo } = newData;

        return new Promise((resolve, reject) => {
            const query = 'UPDATE Colores SET background = ?, headerBackground = ?, sectionBackground = ?, activo = ? WHERE integranteId = ?';
            db.run(query, [background, headerBackground, sectionBackground, activo, id], function(err) {
                if (err) {
                    console.error('Error al actualizar datos:', err);
                    reject('Error al actualizar los datos');
                }
                resolve();
            });
        });
    },

    delete: function(id) {
        return new Promise((resolve, reject) => {
            const query = `UPDATE Colores SET activo = 0 WHERE integranteId = ?`;
            db.run(query, [id], function(err) {
                if (err) {
                    console.error('Error al eliminar el registro:', err);
                    reject('Error al eliminar el registro');
                }
                resolve();
            });
        });
    }
};

module.exports = ColoresModel;
