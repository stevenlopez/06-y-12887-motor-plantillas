/*
getAll - obtener todos los registros - filtros opcionales de busqueda deben estar por
cada campo de la tabla
getById - obtener un registro por Id
getByField - obtener un registro por campo arbitrario
create - crear un registro
update - actualizar registro
delete - borrar un registro
*/
const db = require('../db/conexion');

const mediaModel = {
    getAll: (filters) => {
        return new Promise((resolve, reject) => {
            let sql = `
                SELECT 
                    id, 
                    CONCAT((SELECT nombre FROM integrantes WHERE id = media.integranteid), " - ", (SELECT matricula FROM integrantes WHERE id = media.integranteid)) AS integranteId, 
                    (SELECT nombre FROM tiposmedia WHERE id = media.tiposmediaid) AS tiposmediaId, 
                    URL, 
                    nombrearchivo, 
                    orden 
                FROM 
                    Media 
                WHERE 
                    activo = 1 
            `;
            let queryParams = [];
        
            // Construcción dinámica de la consulta SQL
            if (filters.id) {
                sql += " AND id = ?";
                queryParams.push(filters.id);
            }
            if (filters.integrante) {
                sql += " AND (CONCAT((SELECT nombre FROM integrantes WHERE id = media.integranteid), ' - ', (SELECT matricula FROM integrantes WHERE id = media.integranteid)) LIKE ?)";
                queryParams.push('%' + filters.integrante + '%');
            }
            if (filters.tipomedia) {
                sql += " AND (SELECT nombre FROM tiposmedia WHERE id = media.tiposmediaid) LIKE ?";
                queryParams.push('%' + filters.tipomedia + '%');
            }
            if (filters.orden) {
                sql += " AND orden LIKE ?";
                queryParams.push('%' + filters.orden + '%');
            }
        
            sql += " ORDER BY orden";
        
            db.all(sql, queryParams, (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
            });
        });
    },

    getById: (id) => {
        return new Promise((resolve, reject) => {
            db.get('SELECT * FROM Media WHERE id = ?', [id], (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        });
    },

    getByField: (field, value) => {
        return new Promise((resolve, reject) => {
            const query = `SELECT * FROM Media WHERE ${field} = ?`;
            db.get(query, [value], (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        });
    },

    create: (data) => {
        return new Promise((resolve, reject) => {
            const { integranteId, tiposmediaId, url, nombrearchivo, orden, activo } = data;
            const query = `INSERT INTO Media (integranteId, tiposmediaId, url, nombrearchivo, orden, activo) VALUES (?, ?, ?, ?, ?, ?)`;
            db.run(query, [integranteId, tiposmediaId, url, nombrearchivo, orden, activo], function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve(this.lastID);
                }
            });
        });
    },

    update: (id, data) => {
        return new Promise((resolve, reject) => {
            const { url, nombrearchivo, orden, activo } = data;
            const query = `UPDATE Media SET url = ?, nombrearchivo = ?, orden = ?, activo = ? WHERE id = ?`;
            db.run(query, [url, nombrearchivo, orden, activo, id], function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve(this.changes);
                }
            });
        });
    },

    delete: (id) => {
        return new Promise((resolve, reject) => {
            const query = `UPDATE Media SET activo = 0 WHERE id = ?`;
            db.run(query, [id], function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve(this.changes);
                }
            });
        });
    }
};

module.exports = mediaModel;
