const ColoresModel = require('../../models/Colores.models');
const db = require('../../db/conexion');  // Asegúrate de importar la conexión de la base de datos

const ColoresController = {
    index: async function (req, res) {
        try {
            const results = await ColoresModel.getAll(req.query);
            res.render("admin/colores/index", {
                colores: results,
                mostrarAdmin: true,
                footerfijo: true
            });
        } catch (error) {
            console.error('Error al obtener datos:', error);
            res.status(500).send('Error al obtener datos de la base de datos');
        }
    },

    create: async (req, res) => {
        try {
            const integrantesSinColores = await new Promise((resolve, reject) => {
                db.all(`
                    SELECT i.id, i.nombre
                    FROM Integrantes i
                    LEFT JOIN Colores c ON i.id = c.integranteId
                    WHERE c.integranteId IS NULL;
                `, (err, rows) => {
                    if (err) reject(err);
                    resolve(rows);
                });
            });

            res.render('admin/colores/crearColores', {
                integrantes: integrantesSinColores,
                background: req.query.background || '',
                headerBackground: req.query.headerBackground || '',
                sectionBackground: req.query.sectionBackground || '',
                error: req.flash('error'),
                success: req.flash('success')
            });
        } catch (error) {
            console.error('Error al obtener integrantes sin colores:', error);
            req.flash('error', 'Error al obtener la lista de integrantes sin colores.');
            res.redirect('/ruta/de/error');
        }
    },

    store: async (req, res) => {
        try {
            const { integranteId, background, headerBackground, sectionBackground } = req.body;

            if (!integranteId || !background || !headerBackground || !sectionBackground) {
                req.flash('error', 'Todos los campos son obligatorios.');
                return res.redirect(`/admin/colores/crear?integranteId=${encodeURIComponent(integranteId)}&background=${encodeURIComponent(background)}&headerBackground=${encodeURIComponent(headerBackground)}&sectionBackground=${encodeURIComponent(sectionBackground)}`);
            }

            await ColoresModel.create({
                integranteId,
                background,
                headerBackground,
                sectionBackground
            });

            req.flash('success', 'Colores asignados correctamente!');
            res.redirect('/admin/colores/listar');
        } catch (error) {
            req.flash('error', 'Error al insertar en la base de datos.');
            res.redirect(`/admin/colores/crear?integranteId=${encodeURIComponent(req.body.integranteId)}&background=${encodeURIComponent(req.body.background)}&headerBackground=${encodeURIComponent(req.body.headerBackground)}&sectionBackground=${encodeURIComponent(req.body.sectionBackground)}`);
        }
    },

    edit: async (req, res) => {
        const id = req.params.id;
        try {
            const color = await ColoresModel.getById(id);
            res.render('admin/colores/editarColores', {
                color,
                success: req.flash('success')
            });
        } catch (error) {
            console.error('Error al obtener datos:', error);
            req.flash('error', 'Error al obtener los datos del registro.');
            res.redirect('/admin/colores/listar');
        }
    },

    update: async (req, res) => {
        const id = req.params.id;
        const { background, headerBackground, sectionBackground } = req.body;

        if (!background || !headerBackground || !sectionBackground) {
            req.flash('error', 'Todos los campos son obligatorios.');
            return res.redirect(`/admin/colores/${id}/editar`);
        }

        try {
            await ColoresModel.update(id, {
                background,
                headerBackground,
                sectionBackground,
                activo: req.body.activo ? 1 : 0
            });

            req.flash('success', 'Colores actualizados correctamente!');
            res.redirect(`/admin/colores/listar`);
        } catch (error) {
            req.flash('error', 'Error al actualizar en la base de datos.');
            res.redirect(`/admin/colores/${id}/editar`);
        }
    },
    
    destroy: async (req, res) => {
        const id = req.params.id;

        try {
            await ColoresModel.delete(id);
            req.flash('success', 'Color eliminado correctamente!');
            res.redirect('/admin/colores/listar');
        } catch (error) {
            console.error('Error al eliminar el registro:', error);
            req.flash('error', 'Error al eliminar el registro.');
            res.redirect('/admin/colores/listar');
        }
    }
};

module.exports = ColoresController;
