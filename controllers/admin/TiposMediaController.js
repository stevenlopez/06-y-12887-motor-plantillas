const path = require('path');
const fs = require('fs');
const tipoMediaModel = require('../../models/TiposMedia.models');

async function getNextOrder(tableName) {
    return new Promise((resolve, reject) => {
        const query = `SELECT MAX(orden) as maxOrder FROM ${tableName}`;
        db.get(query, (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row.maxOrder ? row.maxOrder + 1 : 1); // Si no hay registros, empezamos desde 1
            }
        });
    });
}

const TiposMediaController = {
    // Método para listar los tipos de media
    index: async (req, res) => {
        try {
            const filters = req.query;
            const results = await tipoMediaModel.getAll(filters);
            res.render("admin/tiposmedia/index", {
                tiposMedia: results,
                mostrarAdmin: true,
                footerfijo: true
            });
        } catch (err) {
            console.error('Error al obtener datos:', err);
            res.status(500).send('Error al obtener datos de la base de datos');
        }
    },

    // Método para mostrar el formulario de creación
    create: (req, res) => {
        res.render('admin/tiposmedia/creartipomedia', {
            nombre: req.query.nombre || '',
            orden: req.query.orden || '',
            activo: req.query.activo === '1',
            error: req.flash('error'),
            success: req.flash('success')
        });
    },

    // Método para guardar en la base de datos
    store: async (req, res) => {
        const { nombre } = req.body;
        const activo = req.body.activo ? 1 : 0;

        const orden = await getNextOrder('TiposMedia');
        if (!nombre) {
            req.flash('error', 'Todos los campos son obligatorios.');
            return res.redirect(`/admin/tiposmedia/crear?nombre=${encodeURIComponent(nombre)}&orden=${encodeURIComponent(orden)}&activo=${activo}`);
        }

        const newTipoMedia = { nombre, activo, orden };

        try {
            await tipoMediaModel.create(newTipoMedia);
            req.flash('success', 'Tipo de media creado correctamente! Espera a que los programadores implementen la funciondalidad para usarlo');
            res.redirect('/admin/tiposmedia/listar');
        } catch (err) {
            req.flash('error', 'Error al insertar en la base de datos.');
            res.redirect(`/admin/tiposmedia/crear?nombre=${encodeURIComponent(nombre)}&orden=${encodeURIComponent(orden)}&activo=${activo}`);
        }
    },

    // Método para mostrar el formulario de edición
    edit: async (req, res) => {
        const id = req.params.id;

        try {
            const tiposmedia = await tipoMediaModel.getById(id);

            if (!tiposmedia) {
                req.flash('error', 'Tipo de media no encontrado.');
                return res.redirect('/admin/tiposmedia/listar');
            }

            res.render('admin/tiposmedia/editarTipoMedia', {
                tiposmedia
            });
        } catch (error) {
            console.error('Error al obtener datos:', error);
            req.flash('error', 'Error al obtener datos de la base de datos.');
            res.redirect('/admin/tiposmedia/listar');
        }
    },

    // Método para actualizar el registro
    update: async (req, res) => {
        const { id, nombre } = req.body;
        const activo = req.body.activo ? 1 : 0;

        try {
            const updateData = { nombre, activo };
            await tipoMediaModel.update(id, updateData);
            req.flash('success', 'Tipo de media actualizado correctamente!');
            res.redirect(`/admin/tiposmedia/listar`);
        } catch (error) {
            console.error('Error al realizar consultas:', error);
            req.flash('error', 'Error al realizar consultas. ' + error.message);
            res.redirect(`/admin/tiposmedia/${id}/editar`);
        }
    },

    // Método para eliminar un registro
    destroy: async (req, res) => {
        const id = req.params.id;

        try {
            const count = await tipoMediaModel.checkUsageInMedia(id);
            if (count > 0) {
                const message = `No se puede eliminar el registro porque está siendo utilizado en la tabla Media (${count} ${count > 1 ? 'veces' : 'vez'}).`;
                req.flash('error', message);
                return res.redirect('/admin/tiposmedia/listar');
            }

            await tipoMediaModel.delete(id);
            req.flash('success', 'Tipo de media eliminado correctamente!');
            res.redirect('/admin/tiposmedia/listar');
        } catch (err) {
            console.error('Error al eliminar el registro:', err);
            req.flash('error', 'Error al eliminar el registro.');
            res.redirect('/admin/tiposmedia/listar');
        }
    }
};

module.exports = TiposMediaController;
