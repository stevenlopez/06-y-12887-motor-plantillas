/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro */

const db = require('../../db/conexion');
const integrantesModel = require('../../models/Integrantes.models');

async function getNextOrder(tableName) {
    return new Promise((resolve, reject) => {
        const query = `SELECT MAX(orden) as maxOrder FROM ${tableName}`;
        db.get(query, (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row.maxOrder ? row.maxOrder + 1 : 1); // Si no hay registros, empezamos desde 1
            }
        });
    });
}

const IntegrantesController = {
    index: async function (req, res) {
        try {
            const results = await integrantesModel.getAll(req.query);
            res.render("admin/integrantes/index", {
                integrantes: results,
                mostrarAdmin: true,
                footerfijo: true     
            });
        } catch (error) {
            console.error('Error al obtener datos:', error);
            res.status(500).send('Error al obtener datos de la base de datos');
        }
    },

    create: async function (req, res) {
        res.render('admin/integrantes/crearIntegrante', {
            integrante: req.session.formData || {},
            error: req.flash('error')
        });
    },

    store: async function (req, res) {
        try {
            const { error } = integranteStoreSchema.validate(req.body, { abortEarly: false });
            if (error) {
                const mensaje = error.details.map(detail => detail.message).join('; ');
                req.flash('error', mensaje);
                return res.redirect('/admin/integrantes/crear');
            }
    
            const { nombre, apellido, matricula, activo } = req.body;
    
            const orden = await integrantesModel.getNextOrder('integrantes');
    
            await integrantesModel.create({
                nombre,
                apellido,
                matricula,
                activo: activo ? 1 : 0,
                orden
            });
    
            req.flash('success', 'Integrante creado correctamente!');
            res.redirect('/admin/integrantes/listar');
        } catch (error) {
            console.error("Error al almacenar el integrante:", error);
            req.flash('error', 'Error al insertar en la base de datos.');
            req.session.formData = req.body;
            res.redirect('/admin/integrantes/crear');
        }
    },
    

    edit: async function (req, res) {
        const id = req.params.id;
        try {
            const row = await integrantesModel.getById(id);
            const formData = req.session.formData || {};
            delete req.session.formData;
            const errors = req.flash('error');
            const integrante = { ...row, ...formData };

            res.render('admin/integrantes/editarIntegrante', {
                integrante: integrante,
                formData: JSON.stringify(formData),
                errors: errors,
                success: req.flash('success')
            });
        } catch (error) {
            console.error('Error al obtener datos:', error);
            res.status(500).send('Error al obtener datos de la base de datos');
        }
    },

    update: async function (req, res) {
        const id = req.params.id;
        const { nombre, apellido, matricula, activo } = req.body;

        try {
            if (!nombre || !apellido || !matricula) {
                req.flash('error', 'Todos los campos son obligatorios');
                req.session.formData = req.body;
                return res.redirect(`/admin/integrantes/${id}/editar`);
            }

            const activoEstado = activo === 'on' ? 1 : 0;

            await integrantesModel.update(id, {
                nombre,
                apellido,
                matricula,
                activo: activoEstado
            });

            req.flash('success', 'Integrante actualizado correctamente');
            res.redirect('/admin/integrantes/listar');
        } catch (error) {
            console.error('Error al actualizar datos:', error);
            req.flash('error', 'Error al actualizar los datos');
            req.session.formData = req.body;
            res.redirect(`/admin/integrantes/${id}/editar`);
        }
    },

    destroy: async function (req, res) {
        const id = req.params.id;

        try {
            await integrantesModel.delete(id);
            req.flash('success', 'Integrante eliminado correctamente!');
            res.redirect('/admin/integrantes/listar');
        } catch (error) {
            console.error('Error al eliminar el registro:', error);
            req.flash('error', 'Error al eliminar el registro.');
            res.redirect('/admin/integrantes/listar');
        }
    }
};

module.exports = IntegrantesController;