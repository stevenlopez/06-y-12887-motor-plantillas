/*
getAll - obtener todos los registros - filtros opcionales de busqueda deben estar por
cada campo de la tabla
getById - obtener un registro por Id
getByField - obtener un registro por campo arbitrario
create - crear un registro
update - actualizar registro
delete - borrar un registro
*/
const db = require('../db/conexion');

const integrantesModel = {
    getAll: function(queryParams) {
        let sql = "SELECT * FROM integrantes WHERE activo = 1";
        let queryParamsArray = [];

        // Construcción dinámica de la consulta SQL
        if (queryParams.id) {
            sql += " AND id = ?";
            queryParamsArray.push(queryParams.id);
        }
        if (queryParams.nombre) {
            sql += " AND nombre LIKE ?";
            queryParamsArray.push('%' + queryParams.nombre + '%');
        }
        if (queryParams.apellido) {
            sql += " AND apellido LIKE ?";
            queryParamsArray.push('%' + queryParams.apellido + '%');
        }
        if (queryParams.matricula) {
            sql += " AND matricula LIKE ?";
            queryParamsArray.push('%' + queryParams.matricula + '%');
        }
        if (queryParams.orden) {
            sql += " AND orden = ?";
            queryParamsArray.push(queryParams.orden);
        }

        return new Promise((resolve, reject) => {
            db.all(sql, queryParamsArray, (err, results) => {
                if (err) {
                    console.error('Error al obtener datos:', err);
                    reject('Error al obtener datos de la base de datos');
                }
                resolve(results);
            });
        });
    },

    getById: function(id) {
        return new Promise((resolve, reject) => {
            db.get('SELECT * FROM integrantes WHERE id = ?', [id], (err, row) => {
                if (err) {
                    console.error('Error al obtener datos:', err);
                    reject('Error al obtener datos de la base de datos');
                }
                resolve(row);
            });
        });
    },

    getByField: function(fieldName, value) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM integrantes WHERE ${fieldName} = ?`, [value], (err, row) => {
                if (err) {
                    console.error('Error al obtener datos:', err);
                    reject('Error al obtener datos de la base de datos');
                }
                resolve(row);
            });
        });
    },

    create: async function(data) {
        const { nombre, apellido, matricula, activo, orden } = data;

        return new Promise((resolve, reject) => {
            const query = `INSERT INTO integrantes (nombre, apellido, matricula, activo, orden) VALUES (?, ?, ?, ?, ?)`;
            db.run(query, [nombre, apellido, matricula, activo, orden], function(err) {
                if (err) {
                    let errorMessage = 'Error al insertar en la base de datos.';
                    if (err.message && err.message.includes('UNIQUE constraint failed: integrantes.matricula')) {
                        errorMessage = 'Error al insertar en la base de datos: La matrícula ya existe.';
                    }
                    reject(errorMessage);
                }
                resolve(this.lastID);
            });
        });
    },

    update: function(id, newData) {
        const { nombre, apellido, matricula, activo, orden } = newData;

        return new Promise((resolve, reject) => {
            const query = 'UPDATE integrantes SET nombre = ?, apellido = ?, matricula = ?, activo = ?, orden = ? WHERE id = ?';
            db.run(query, [nombre, apellido, matricula, activo, orden, id], function(err) {
                if (err) {
                    console.error('Error al actualizar datos:', err);
                    reject('Error al actualizar los datos');
                }
                resolve();
            });
        });
    },

    delete: function(id) {
        return new Promise((resolve, reject) => {
            const query = `UPDATE integrantes SET activo = 0 WHERE id = ?`;
            db.run(query, [id], function(err) {
                if (err) {
                    console.error('Error al eliminar el registro:', err);
                    reject('Error al eliminar el registro.');
                }
                resolve();
            });
        });
    }
};

module.exports = integrantesModel;