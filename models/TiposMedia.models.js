const db = require('../db/conexion');

const tipoMediaModel = {
    getAll: (filters) => {
        return new Promise((resolve, reject) => {
            let sql = "SELECT * FROM TiposMedia WHERE activo = 1";
            let queryParams = [];
        
            if (filters.id) {
                sql += " AND id = ?";
                queryParams.push(filters.id);
            }
            if (filters.nombre) {
                sql += " AND nombre LIKE ?";
                queryParams.push('%' + filters.nombre + '%');
            }
            if (filters.orden) {
                sql += " AND orden LIKE ?";
                queryParams.push('%' + filters.orden + '%');
            }
        
            db.all(sql, queryParams, (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
            });
        });
    },
    getById: (id) => {
        return new Promise((resolve, reject) => {
            db.get('SELECT * FROM TiposMedia WHERE id = ?', [id], (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        });
    },
    create: (data) => {
        return new Promise((resolve, reject) => {
            const { nombre, activo, orden } = data;
            const query = `INSERT INTO TiposMedia (nombre, activo, orden) VALUES (?, ?, ?)`;
            db.run(query, [nombre, activo, orden], function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve(this.lastID);
                }
            });
        });
    },
    update: (id, data) => {
        return new Promise((resolve, reject) => {
            const { nombre, activo } = data;
            const query = `UPDATE TiposMedia SET nombre = ?, activo = ? WHERE id = ?`;
            db.run(query, [nombre, activo, id], function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve(this.changes);
                }
            });
        });
    },
    delete: (id) => {
        return new Promise((resolve, reject) => {
            const query = `UPDATE TiposMedia SET activo = 0 WHERE id = ?`;
            db.run(query, [id], function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve(this.changes);
                }
            });
        });
    },
    checkUsageInMedia: (id) => {
        return new Promise((resolve, reject) => {
            const query = 'SELECT COUNT(*) AS count FROM Media WHERE tiposmediaId = ?';
            db.get(query, [id], (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row.count);
                }
            });
        });
    }
};

module.exports = tipoMediaModel;