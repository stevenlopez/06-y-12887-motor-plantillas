const Joi = require("joi");

const integranteStoreSchema = Joi.object({
    nombre: Joi.string()
        .required()
        .min(2)
        .max(50)
        .messages({
            "string.base": `"nombre" debe ser una cadena`,
            "string.empty": `"nombre" es obligatorio`,
            "string.min": `"nombre" debe tener al menos {#limit} caracteres`,
            "string.max": `"nombre" no debe tener más de {#limit} caracteres`,
            "any.required": `"nombre" es obligatorio`
        }),
    apellido: Joi.string()
        .required()
        .min(2)
        .max(50)
        .messages({
            "string.base": `"apellido" debe ser una cadena`,
            "string.empty": `"apellido" es obligatorio`,
            "string.min": `"apellido" debe tener al menos {#limit} caracteres`,
            "string.max": `"apellido" no debe tener más de {#limit} caracteres`,
            "any.required": `"apellido" es obligatorio`
        }),
    matricula: Joi.string()
        .required()
        .min(2)
        .max(20)
        .messages({
            "string.base": `"matricula" debe ser una cadena`,
            "string.empty": `"matricula" es obligatorio`,
            "string.min": `"matricula" debe tener al menos {#limit} caracteres`,
            "string.max": `"matricula" no debe tener más de {#limit} caracteres`,
            "any.required": `"matricula" es obligatorio`
        }),
    activo: Joi.boolean()
        .required()
        .messages({
            "boolean.base": `"activo" debe ser un valor booleano`,
            "any.required": `"activo" es obligatorio`
        })
});

module.exports = integranteStoreSchema;
