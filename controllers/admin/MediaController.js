const db = require('../../db/conexion');
const path = require('path');
const fs = require('fs');
const mediaModel = require('../../models/Media.models');

async function getNextOrder(tableName) {
    const query = `SELECT MAX(orden) as maxOrder FROM ${tableName}`;
    return new Promise((resolve, reject) => {
        db.get(query, (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row.maxOrder ? row.maxOrder + 1 : 1); // Si no hay registros, empezamos desde 1
            }
        });
    });
}

function getYouTubeEmbedUrl(url) {
    const regex = /(?:youtube\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    const match = url.match(regex);
    return match ? `https://www.youtube.com/embed/${match[1]}` : null;
}

const MediaController = {

    index: async (req, res) => {
        try {
            const results = await mediaModel.getAll(req.query);
            res.render("admin/media/index", {
                media: results,
                mostrarAdmin: true,
                footerfijo: true
            });
        } catch (err) {
            console.error('Error al obtener datos:', err);
            res.status(500).send('Error al obtener datos de la base de datos');
        }
    },

    create: async (req, res) => {
        try {
            const allIntegrantes = await new Promise((resolve, reject) => {
                db.all('SELECT id, nombre FROM Integrantes WHERE activo = 1 ORDER BY nombre', (err, rows) => {
                    if (err) reject(err);
                    resolve(rows);
                });
            });

            const allTiposMedia = await new Promise((resolve, reject) => {
                db.all('SELECT id, nombre FROM TiposMedia WHERE activo = 1 ORDER BY nombre', (err, rows) => {
                    if (err) reject(err);
                    resolve(rows);
                });
            });

            const mediaByIntegrante = await new Promise((resolve, reject) => {
                db.all('SELECT integranteId, tiposmediaId FROM Media WHERE activo = 1', (err, rows) => {
                    if (err) reject(err);
                    resolve(rows);
                });
            });

            const tiposMediaMap = {};
            allTiposMedia.forEach(tipo => {
                tiposMediaMap[tipo.id] = tipo.nombre;
            });

            const integrantesFaltantes = [];
            const integrantesMap = {};

            allIntegrantes.forEach(integrante => {
                integrantesMap[integrante.id] = {
                    ...integrante,
                    tiposFaltantes: new Set(Object.values(tiposMediaMap))
                };
            });

            mediaByIntegrante.forEach(media => {
                if (integrantesMap[media.integranteId]) {
                    integrantesMap[media.integranteId].tiposFaltantes.delete(tiposMediaMap[media.tiposmediaId]);
                }
            });

            Object.values(integrantesMap).forEach(integrante => {
                if (integrante.tiposFaltantes.size > 0) {
                    integrantesFaltantes.push({
                        id: integrante.id,
                        nombre: integrante.nombre,
                        tiposFaltantes: Array.from(integrante.tiposFaltantes)
                    });
                }
            });

            res.render('admin/media/crearMedia', { 
                integrantes: integrantesFaltantes
            });
        } catch (error) {
            console.error('Error al obtener datos:', error);
            res.status(500).send('Error al obtener datos de la base de datos');
        }
    },

    store: async (req, res) => {
        const { integranteId, tiposmediaId, url } = req.body;
        const activo = req.body.activo ? 1 : 0;
        const file = req.file;

        const orden = await getNextOrder('media');

        if (!integranteId || !tiposmediaId) {
            req.flash('error', 'Los campos de ID de integrante y tipo de media son obligatorios.');
            return res.redirect('/admin/media/crear');
        }
        
        try {
            const integrante = await new Promise((resolve, reject) => {
                db.get(`SELECT nombre, apellido FROM Integrantes WHERE id = ?`, integranteId, (err, row) => {
                    if (err) reject(err);
                    resolve(row);
                });
            });

            if (!integrante) {
                req.flash('error', 'Integrante no encontrado.');
                return res.redirect('/admin/media/crear');
            }

            const tipoMedia = await new Promise((resolve, reject) => {
                db.get(`SELECT id FROM TiposMedia WHERE nombre = ?`, tiposmediaId, (err, row) => {
                    if (err) reject(err);
                    resolve(row);
                });
            });

            if (!tipoMedia) {
                req.flash('error', 'Tipo de media no encontrado.');
                return res.redirect('/admin/media/crear');
            }

            let finalUrl = url;
            if (tiposmediaId === 'youtube') {
                finalUrl = getYouTubeEmbedUrl(url);
                if (!finalUrl) {
                    req.flash('error', 'URL de YouTube no válida.');
                    return res.redirect('/admin/media/crear');
                }
            }

            const newMedia = {
                integranteId,
                tiposmediaId: tipoMedia.id,
                url: finalUrl,
                nombrearchivo: null,
                orden,
                activo
            };

            if (tiposmediaId !== 'youtube' && file) {
                const newFileName = `${tipoMedia.id}-${integrante.nombre}-${integrante.apellido}-${Date.now()}${path.extname(file.originalname)}`;
                const newFilePath = path.join('public/images', newFileName);

                fs.rename(file.path, newFilePath, (err) => {
                    if (err) {
                        req.flash('error', 'Error al renombrar el archivo. ' + err.message);
                        return res.redirect('/admin/media/crear');
                    }

                    newMedia.nombrearchivo = `/images/${newFileName}`;

                    mediaModel.create(newMedia).then(() => {
                        req.flash('success', 'Media creada correctamente!');
                        res.redirect('/admin/media/listar');
                    }).catch(err => {
                        req.flash('error', 'Error al insertar en la base de datos. ' + err.message);
                        res.redirect('/admin/media/crear');
                    });
                });
            } else {
                mediaModel.create(newMedia).then(() => {
                    req.flash('success', 'Media creada correctamente!');
                    res.redirect('/admin/media/listar');
                }).catch(err => {
                    req.flash('error', 'Error al insertar en la base de datos. ' + err.message);
                    res.redirect('/admin/media/crear');
                });
            }
        } catch (error) {
            console.error('Error al realizar consultas:', error);
            req.flash('error', 'Error al realizar consultas. ' + error.message);
            res.redirect('/admin/media/crear');
        }
    },

    edit: async (req, res) => {
        const id = req.params.id;

        try {
            const media = await mediaModel.getById(id);

            if (!media) {
                req.flash('error', 'Media no encontrada.');
                return res.redirect('/admin/media/listar');
            }

            res.render('admin/media/editarMedia', {
                media
            });
        } catch (error) {
            console.error('Error al obtener datos:', error);
            req.flash('error', 'Error al obtener datos de la base de datos.');
            res.redirect('/admin/media/listar');
        }
    },

    update: async (req, res) => {
        const { id, url, orden, tiposmediaId, integranteId } = req.body;
        const activo = req.body.activo ? 1 : 0;
        const file = req.file;

        try {
            const media = await mediaModel.getById(id);

            if (!media) {
                req.flash('error', 'Media no encontrada.');
                return res.redirect('/admin/media/listar');
            }

            let finalUrl = url;
            if (tiposmediaId === '1') {
                finalUrl = getYouTubeEmbedUrl(url);
                if (!finalUrl) {
                    req.flash('error', 'URL de YouTube no válida.');
                    return res.redirect(`/admin/media/${id}/editar`);
                }
            }

            const updateData = {
                url: finalUrl,
                nombrearchivo: media.nombrearchivo,
                orden,
                activo
            };

            if (file) {
                const newFileName = `${tiposmediaId}-${integranteId}-${Date.now()}${path.extname(file.originalname)}`;
                const newFilePath = path.join('public/images', newFileName);

                fs.rename(file.path, newFilePath, (err) => {
                    if (err) {
                        req.flash('error', 'Error al renombrar el archivo. ' + err.message);
                        return res.redirect(`/admin/media/${id}/editar`);
                    }

                    updateData.nombrearchivo = `/images/${newFileName}`;
                    
                    mediaModel.update(id, updateData).then(() => {
                        req.flash('success', 'Media actualizada correctamente!');
                        res.redirect('/admin/media/listar');
                    }).catch(err => {
                        req.flash('error', 'Error al actualizar en la base de datos. ' + err.message);
                        res.redirect(`/admin/media/${id}/editar`);
                    });
                });
            } else {
                mediaModel.update(id, updateData).then(() => {
                    req.flash('success', 'Media actualizada correctamente!');
                    res.redirect('/admin/media/listar');
                }).catch(err => {
                    req.flash('error', 'Error al actualizar en la base de datos. ' + err.message);
                    res.redirect(`/admin/media/${id}/editar`);
                });
            }
        } catch (error) {
            console.error('Error al realizar consultas:', error);
            req.flash('error', 'Error al realizar consultas. ' + error.message);
            res.redirect(`/admin/media/${id}/editar`);
        }
    },

    destroy: async (req, res) => {
        const id = req.params.id;

        try {
            await mediaModel.delete(id);
            req.flash('success', 'Media eliminada correctamente!');
            res.redirect('/admin/media/listar');
        } catch (err) {
            console.error('Error al eliminar el registro:', err);
            req.flash('error', 'Error al eliminar el registro.');
            res.redirect('/admin/media/listar');
        }
    }
};

module.exports = MediaController;
